import React from 'react';

class Image extends React.Component {


   render() {
     var divStyle = {
     backgroundImage: 'url(' + this.props.bg + ')',
     height: '100%'
     };
      return (

        <div className="img" style={divStyle} >
        </div>
      );
   }


}



export default Image;
