import React from 'react';
import ReactDOM from 'react-dom';
import DragSortableList from 'react-drag-sortable';
import Box from './box';
import Placeholder from './placeholder';
import Rebase from 're-base';
import ImageUpload from './upload'
import '../App.css';


var base = Rebase.createClass({
  apiKey: "AIzaSyDmDmCpFzNhHt_Z-bizH-72EGIJeNmBkgg",
   authDomain: "nyne-bc248.firebaseapp.com",
   databaseURL: "https://nyne-bc248.firebaseio.com",
   storageBucket: "nyne-bc248.appspot.com",
   messagingSenderId: "938488675521"
});

var storageRef = base.storage().ref(); 

var addPosts = function(list){

  var myJSON = JSON.stringify(list);

  base.post(`grid`, {
    data: {myJSON},
    then(err){


    }
  });

}

let foo = [];
let placeholders = [];

var onSort = function(sortedList) {
      foo = sortedList;
      addPosts(sortedList);
}

function Loader(){
  return '<h1>Loading</h1>';
}


for(var i = 0; i < 9; i++){
  placeholders.push({content: <Placeholder />});
}

class Grid extends React.Component{



  constructor(props){
    super(props);
    this.state = {
      data: [],
      loading: true
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleImage = this.handleImage.bind(this);
  }


handleClick(a, b){


var searchTerm = b;
for(var i = 0; i < foo.length; i++) {
    if (foo[i].rank == searchTerm) {
        foo[i] = {content: <Box name={a} handleClick={this.handleClick} handleImage={this.handleImage}/>, rank: b, time: base.database.ServerValue.TIMESTAMP};

        this.setState({data: foo});
  break;
    }

}


addPosts(foo);

}

handleImage(file, b){
  var self = this;
var url;
  var storageRef = base.storage().ref();
  storageRef.child('images/' + b).put(file).then(function(snapshot) {

  url = snapshot.downloadURL;
  var pos = foo.map(function(e) { return e.rank; }).indexOf(parseInt(b, 10));

      foo[pos] = {content: (<Box bg={url} handleClick={self.handleClick} handleImage={self.handleImage}/>), rank: b};

      //foo.push({content: (<Box name={url}  handleImage={self.handleImage}/>), rank: b});
self.setState({data: foo});
addPosts(foo);




  }).catch(function(error) {
  console.error('Upload failed:', error);
  });



}

getPosts(){
    base.fetch('grid', {
      context: this,
      asArray: true
    }).then(data => {
      var result = JSON.parse(data);
      for (var i = 0; i < 9; i++) {

          if(result[i])
          {
        foo.push({content: <Box name={result[i].content.props.name} bg={result[i].content.props.bg} handleClick={this.handleClick.bind(this)} handleImage={this.handleImage.bind(this)}/>, rank: result[i].rank});
}
else{
  foo.push({content: <Box name={''} bg={''} handleClick={this.handleClick.bind(this)} handleImage={this.handleImage.bind(this)}/>});

}
  this.setState({data: foo});
  this.setState({loading: false});

      }

    }).catch(error => {
      //handle error
    })
}

componentDidMount(){


this.getPosts();





}

componentDidUpdate(){
  var boxes = document.getElementsByClassName("draggable");
var height;
  for (var i = 0; i < boxes.length; i++) {
    boxes[i].style.height = window.getComputedStyle(boxes[i]).width;
    height = window.getComputedStyle(boxes[i]).width;
  }

  var boxes = document.getElementsByClassName("placeholder");

  for (var i = 0; i < boxes.length; i++) {
    boxes[i].style.height = height;
  }
}


  render(){
    var boxes = document.getElementsByClassName("draggable");

    for (var i = 0; i < boxes.length; i++) {
      boxes[i].style.height = window.getComputedStyle(boxes[i]).width;
    }


if(this.state.loading)
var Grid = <DragSortableList onSort={onSort} items={placeholders} dropBackTransitionDuration={1} moveTransitionDuration={0.5} type="grid"/>;
else
    var Grid = <DragSortableList onSort={onSort} items={this.state.data} dropBackTransitionDuration={0.5} moveTransitionDuration={0.8} type="grid"/>;


    return (
      <div>
        {Grid}
      </div>
    )
  }
};


export default Grid;
