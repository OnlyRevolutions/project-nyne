import React from 'react';
import ReactDOM from 'react-dom';
import DragSortableList from 'react-drag-sortable';
import Box from './box';
import Rebase from 're-base';
import ImageUpload from './upload'
import '../App.css';

var base = Rebase.createClass({
  apiKey: "AIzaSyDmDmCpFzNhHt_Z-bizH-72EGIJeNmBkgg",
   authDomain: "nyne-bc248.firebaseapp.com",
   databaseURL: "https://nyne-bc248.firebaseio.com",
   storageBucket: "nyne-bc248.appspot.com",
   messagingSenderId: "938488675521"
});
var storageRef = base.storage().ref();

var addPosts = function(list){

  var myJSON = JSON.stringify(list);

  base.post(`grid`, {
    data: {myJSON},
    then(err){

    }
  });
}

var onSort = function(sortedList) {
   console.log("sortedList", sortedList);
   addPosts(sortedList);
}

let foo = [];
class Grid extends React.Component{


  constructor(props){
    super(props);
    this.state = {
      data: []
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleImage = this.handleImage.bind(this);
  }

handleClick(a, b, c){
console.log(a);
console.log(b);
console.log(c);

var newArray = foo.slice();

for(var i = 0; i < newArray.length; i++)
{
  if(foo[i].rank == b)
  {
    foo[i] = {content: (<Box name={a} handleClick={this.handleClick} handleImage={this.handleImage}/>), rank: b};
    this.setState({data: foo});
  }
}
addPosts(foo);
}

handleImage(file, b, c){
  var self = this;
var url;
  var storageRef = base.storage().ref();
  storageRef.child('images/' + "image1.jpg").put(file).then(function(snapshot) {

  url = snapshot.downloadURL;
  for(var i = 0; i < 2; i++)
  {
    if(foo[i].rank == b)
    {
      console.log(b);
      foo[i] = {content: (<Box name={url}  handleClick={self.handleClick} handleImage={self.handleImage}/>), rank: b};
      //foo.push({content: (<Box name={url}  handleImage={self.handleImage}/>), rank: b});

    }

  }
  self.setState({data: foo});
  }).catch(function(error) {
  console.error('Upload failed:', error);
  });


//  addPosts(foo);
}

getPosts(){
    base.fetch('grid', {
      context: this,
      asArray: true
    }).then(data => {
      var result = JSON.parse(data);
console.log(result);
      for (var i = 0; i < result.length; i++) {

          console.log(result[i]);
        foo.push({content: <Box name={result[i].content.props.name} handleClick={this.handleClick.bind(this)} handleImage={this.handleImage.bind(this)}/>, rank: result[i].rank});

  this.setState({data: foo});
      }

    }).catch(error => {
      //handle error
    })
}


componentWillMount(){
}

componentWillUnmount(){


}

componentDidMount(){

this.getPosts();


}


  render(){
    return (
      <div>
      <DragSortableList onSort={onSort} items={this.state.data} dropBackTransitionDuration={1} moveTransitionDuration={0.5} type="grid"/>
      </div>
    )
  }
};


export default Grid;
