import React from 'react';
import ReactDOM from 'react-dom';

class Post extends React.Component{


  handleSubmit(){
    var newNote = ReactDOM.findDOMNode(this.refs.note).value;
    ReactDOM.findDOMNode(this.refs.note).value = '';
    this.props.addNote(newNote);
  }

  constructor(props){
    super(props);
  }

componentWillMount(){
}

componentDidMount(){


}


  render(){
    return (
        <input type="text" className="form-control" ref="note" placeholder="Add New Note" />
    )
  }
};


export default Post;
