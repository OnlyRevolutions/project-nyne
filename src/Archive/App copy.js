import React from 'react';
import ReactDOM from 'react-dom';
import Grid from './components/grid';
import Header from './components/header';



class App extends React.Component{

  render(){
    return (
      <div>
        <Header />
        <Grid />
      </div>
    )
  }
};


export default App;
