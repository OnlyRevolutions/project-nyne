import React from 'react';
import ReactDOM from 'react-dom';
import Grid from './components/grid';
import Header from './components/header';
import Profile from './components/pages/profile';
import Login from './components/pages/login';
import BookList from './components/containers/book-list';
import Rebase from 're-base';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

var base = Rebase.createClass({
  apiKey: "AIzaSyDmDmCpFzNhHt_Z-bizH-72EGIJeNmBkgg",
   authDomain: "nyne-bc248.firebaseapp.com",
   databaseURL: "https://nyne-bc248.firebaseio.com",
   storageBucket: "nyne-bc248.appspot.com",
   messagingSenderId: "938488675521"
});

class App extends React.Component{

  render(){
    return(
      <Router history={browserHistory}>
        <Route path="/" component={Login}>
          <IndexRoute component={Login} />
        </Route>
        <Route path="/profile(/:user)" component={Profile} />
        <Route path="/login" component={Login} />
      </Router>
    )
  }
};

export default App;
