import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Login from './components/pages/login';
import './index.css';
import { Router, Route, browserHistory } from 'react-router'


ReactDOM.render(
  <App/>,
  document.getElementById('root'));
