import React from 'react';
import ReactDOM from 'react-dom';
import DragSortableList from 'react-drag-sortable';
import Box from './box';
import Placeholder from './placeholder';
import Rebase from 're-base';
import ImageUpload from './upload'
import '../App.css';


var base = Rebase.createClass({
  apiKey: "AIzaSyDmDmCpFzNhHt_Z-bizH-72EGIJeNmBkgg",
   authDomain: "nyne-bc248.firebaseapp.com",
   databaseURL: "https://nyne-bc248.firebaseio.com",
   storageBucket: "nyne-bc248.appspot.com",
   messagingSenderId: "938488675521"
});

var uid;

var storageRef = base.storage().ref();

let foo = [];
let profileImg;
let placeholders = [];



function Loader(){
  return '<h1>Loading</h1>';
}


for(var i = 0; i < 9; i++){
  placeholders.push({content: <Placeholder />});
}

class Grid extends React.Component{



  constructor(props){
    super(props);
    this.state = {
      data: [],
      loading: true,
      currentUser:null,
      profileImage: null
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleImage = this.handleImage.bind(this);
this.onSort = this.onSort.bind(this);
this.getPosts = this.getPosts.bind(this);
  }



  addPosts(list){
    console.log(this.state.profileImage);
    var grid = JSON.stringify(list);
    base.post(`users/` + this.state.currentUser, {
      data: {grid: grid, fbImage: this.state.profileImage},
      then(err){
      }
    });
  }

  onSort(sortedList) {
    foo = sortedList;
    //console.log(this.props.user);
    this.addPosts(sortedList);
  }


handleClick(a, b){
var searchTerm = b;
for(var i = 0; i < foo.length; i++) {
    if (foo[i].rank == searchTerm) {
        foo[i] = {content: <Box profileImage={this.state.profileImage} name={a} handleClick={this.handleClick} handleImage={this.handleImage}/>, rank: b, time: base.database.ServerValue.TIMESTAMP};

        this.setState({data: foo});
  break;
    }
}

this.addPosts(foo);

}

handleImage(file, b){
  var self = this;
var url;
  var storageRef = base.storage().ref();
  storageRef.child('images/' + b).put(file).then(function(snapshot) {

  url = snapshot.downloadURL;
  var pos = foo.map(function(e) { return e.rank; }).indexOf(parseInt(b, 10));

      foo[pos] = {content: (<Box profileImage={self.state.profileimage}  bg={url} handleClick={self.handleClick} handleImage={self.handleImage}/>), rank: b};

      //foo.push({content: (<Box name={url}  handleImage={self.handleImage}/>), rank: b});
self.setState({data: foo});
self.addPosts(foo);




  }).catch(function(error) {
  console.error('Upload failed:', error);
  });



}

getPosts(){
console.log(this.state.currentUser);
foo = [];

    base.fetch('users/' + this.state.currentUser, {
      context: this,
      asArray: false
    }).then(data => {


      if(data.fbImage){
        profileImg = data.fbImage;
      }
      else{
        profileImg = null;
      }

      if(data.grid.length)
      var result = JSON.parse(data.grid);
      else
      var result = null;


      for (var i = 0; i < 9; i++) {

          if(result.length)
          {

if(base.getAuth())
{
  if(base.getAuth().uid == this.state.currentUser)
  {
    foo.push({content: <Box profileImage={base.getAuth().photoURL} name={result[i].content.props.name} bg={result[i].content.props.bg} handleClick={this.handleClick.bind(this)} handleImage={this.handleImage.bind(this)}/>, rank: result[i].rank});

  }
}
else{

        foo.push({content: <Box profileImage={data.fbImage}  name={result[i].content.props.name} bg={result[i].content.props.bg}/>, rank: result[i].rank});
      }
}
else{
console.log('well shit');
  if(base.getAuth())
  {
    if(base.getAuth().uid == this.state.currentUser)
    {
      foo.push({content: <Box profileImage={base.getAuth().photoURL}  name={''} bg={''} handleClick={this.handleClick.bind(this)} handleImage={this.handleImage.bind(this)}/>});

    }
  }
  else
  {
  foo.push({content: <Box profileImage={data.fbImage}  name={''} bg={''}/>});
}
}
  this.setState({data: foo, profileImage: profileImg});
  this.setState({loading: false});

      }

    }).catch(error => {

    })
}

componentDidMount(){


this.setState({
  currentUser: this.props.user
},
this.getPosts
);









}

componentWillUnmount(){








}

componentDidUpdate(){
  var boxes = document.getElementsByClassName("draggable");
var height;
  for (var i = 0; i < boxes.length; i++) {
    boxes[i].style.height = window.getComputedStyle(boxes[i]).width;
    height = window.getComputedStyle(boxes[i]).width;
  }

  var boxes = document.getElementsByClassName("placeholder");

  for (var i = 0; i < boxes.length; i++) {
    boxes[i].style.height = height;
  }
}


  render(){
    var boxes = document.getElementsByClassName("draggable");

    for (var i = 0; i < boxes.length; i++) {
      boxes[i].style.height = window.getComputedStyle(boxes[i]).width;
    }


if(this.state.loading)
var Grid = <DragSortableList onSort={this.onSort} items={placeholders} dropBackTransitionDuration={1} moveTransitionDuration={0.5} type="grid"/>;
else{
  if(base.getAuth())
    {
      if(base.getAuth().uid == this.state.currentUser){
        console.log(base.getAuth());
        var Grid = <DragSortableList onSort={this.onSort} items={this.state.data}  moveTransitionDuration={0.8} type="grid"/>;
      }
    }
    else
  var Grid = <DragSortableList items={this.state.data}  moveTransitionDuration={0.8} type="grid"/>;
}

    return (
      <div>
        {Grid}
      </div>
    )
  }
};


export default Grid;
