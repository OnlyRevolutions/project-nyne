import React from 'react';
import ReactDOM from 'react-dom';
import DragSortableList from 'react-drag-sortable';
import Post from './post';

var profileimage = require('../icons/FB_IMG_1482866903085.jpg');
var statusicon = require('../icons/status-icon.png');
var editicon = require('../icons/edit.png');
var imgicon = require('../icons/image-icon.png');

class Box extends React.Component{



  constructor(props){
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleImage = this.handleImage.bind(this);
    this.showStatus = this.showStatus.bind(this);
    this.showImage = this.showImage.bind(this);
    this.editStatus = this.editStatus.bind(this);
    this.hideStatus = this.hideStatus.bind(this);
    this.state = { imageStatus: null, options: 'initial' };
  }

  handleImageLoaded() {

    ReactDOM.findDOMNode(this.refs.box).style.opacity = '1';




  }

  handleImageErrored() {
    console.log('error');
    //ReactDOM.findDOMNode(this.refs.box).style.opacity = '1';


    this.setState({ imageStatus: 'failed to load' });
  }

  handleClick(){

    this.setState({options: 'initial'});
    this.props.handleClick(ReactDOM.findDOMNode(this.refs.myText).value, ReactDOM.findDOMNode(this.refs.box).parentElement.getAttribute('data-rank'));
    console.log(ReactDOM.findDOMNode(this.refs.box).parentElement.getAttribute('data-rank'));

  }

  handleImage(){
    let file = ReactDOM.findDOMNode(this.refs.myImg).files[0];

    //this.props.handleClick(file);
    this.props.handleImage(file, ReactDOM.findDOMNode(this.refs.box).parentElement.getAttribute('data-rank'));

    ReactDOM.findDOMNode(this.refs.status).value == '';
  }

  editStatus(){
    if(ReactDOM.findDOMNode(this.refs.status))
this.setState({options: 'edit'});
  }

  showStatus(){
this.setState({options: 'status'});
console.log(this.state.options)
  }

  showImage(){
    this.setState({options: 'image'})

  }

  hideStatus(){
    ReactDOM.findDOMNode(this.refs.iconsContain).style.display = 'flex';
    ReactDOM.findDOMNode(this.refs.statusInput).style.display = 'none';
    ReactDOM.findDOMNode(this.refs.cancelIcon).style.display = 'none';

    ReactDOM.findDOMNode(this.refs.imageInput).style.display = 'none';
    ReactDOM.findDOMNode(this.refs.cancelIcon).style.display = 'none';
  }



componentDidMount(){

  var boxes = document.getElementsByClassName("status");
  for (var i = 0; i < boxes.length; i++) {
      boxes[i].style.opacity = "1";
  }





}

hideShow(){
  this.handleClick();
  this.hideStatus();
}

  render(){

if(this.props.bg)
var visible = "0";
else {
  var visible = "1";
}

    var divStyle = {
      height: '100%',
       backgroundSize: 'cover',
       backgroundImage: 'url(' + this.props.bg + ')',
       backgroundPosition: 'center',
       backgroundColor: this.props.load,
       opacity: visible,
    };

    var profileImageStyle = {
       backgroundSize: 'cover',
       backgroundImage: 'url(' + this.props.profileImage + ')',
       backgroundPosition: 'center',
    };



    var hiddenStyle = {
      display: 'none',
    };

    if(this.props.name)
    var status = <div className="status-contain"><div className="profile-image" style={profileImageStyle}></div><p ref="status" className="status">{this.props.name}</p></div>;
    else
    var status = null;

if(this.state.options == 'status')
var showOptions = (<div className="options-wrapper">
        <div ref="statusInput" className="status-input">
        <textarea placeholder="Say something!" className="status-field" type="text" ref="myText" maxLength="120"></textarea>
        <button className="submit-btn" ref="submit" onClick={this.handleClick}>Submit</button>
        </div>
        </div>);
        else if(this.state.options == 'edit'){
          var showOptions = (<div className="options-wrapper">
                  <div ref="statusInput" className="status-input">
                  <textarea placeholder="Say something!" className="status-field" type="text" ref="myText" maxLength="120">{this.props.name}</textarea>
                  <button className="submit-btn" ref="submit" onClick={this.handleClick}>Submit</button>
                  </div>
                  </div>);
        }
        else if(this.state.options == 'image'){
          var showOptions = (<div className="options-wrapper"><div ref="imageInput" className="image-input">
                  <input placeholder="Upload an image..." type="file" id="img" name="img" ref="myImg"/>
                  <button className="submit-btn" onClick={this.handleImage}>Submit</button>
                  </div></div>);
        }
        else if(this.state.options == 'initial'){
          if(this.props.name)
        var showOptions =  <div ref="iconsContain" className="icons-contain">
          <img ref="statusIcon" onClick={this.showStatus} src={statusicon}/>
          <img ref="editIcon" onClick={this.editStatus} src={editicon}/>
          <img ref="imageIcon" onClick={this.showImage} src={imgicon}/>
          </div>;
          else {
            var showOptions = <div ref="iconsContain" className="icons-contain">
            <img ref="statusIcon" onClick={this.showStatus} src={statusicon}/>
            <img ref="imageIcon" onClick={this.showImage} src={imgicon}/>
            </div>;
          }

        }

    return (
      <div ref="box" className="box" style={divStyle}>
        <img className="status-image" style={hiddenStyle} src={this.props.bg} onLoad={this.handleImageLoaded.bind(this)} onError={this.handleImageErrored.bind(this)}/>
        {status}
        <div className="options-contain">

        {showOptions}


        </div>
      </div>
    )
  }
};


Box.propTypes = {
  myFunc: React.PropTypes.func,
};

export default Box;
