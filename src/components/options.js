import React from 'react';
import ReactDOM from 'react-dom';
import Status from './status';

var profileimage = require('../icons/FB_IMG_1482866903085.jpg');
var statusicon = require('../icons/status-icon.png');
var editicon = require('../icons/edit.png');
var imgicon = require('../icons/image-icon.png');

class Options extends React.Component{

constructor(props){
  super(props);
  this.state = {options: 'initial'};
}

showStatus(){
this.setState({options: 'status'});
}

render(){
  if(this.state.options == 'status'){
    var currentOptions = <Status />;
  }
  else {
    var currentOptions = <div ref="iconsContain" className="icons-contain">
      <img ref="statusIcon" onClick={this.showStatus} src={statusicon}/>
      <img ref="editIcon" onClick={this.editStatus} src={editicon}/>
      <img ref="imageIcon" onClick={this.showImage} src={imgicon}/>
    </div>;
  }

  return(
    {currentOptions}
);
}
}
export default Options;
