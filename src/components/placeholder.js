import React from 'react';
import ReactDOM from 'react-dom';
import DragSortableList from 'react-drag-sortable';

var statusicon = require('../icons/status-icon.png');
var imgicon = require('../icons/image-icon.png');



var height;

class Placeholder extends React.Component{



  constructor(props){
    super(props);


  }

componentDidMount(){
  var boxes = document.getElementsByClassName("draggable");


  for (var i = 0; i < boxes.length; i++) {

      boxes[i].style.height = window.getComputedStyle(boxes[i]).width;
  }
}


  render(){



    var divStyle = {
      transform: 'scale(0.2)'
    };

    return (
      <div ref="box" className="placeholder-box">

<div style={divStyle} className='uil-cube-css'><div></div><div></div><div></div><div></div></div>

      </div>
    )
  }
};




export default Placeholder;
