import React from 'react';
import ReactDOM from 'react-dom';
import Header from '../header';
import DragSortableList from 'react-drag-sortable';
import Box from '../box';
import Rebase from 're-base';
import { browserHistory } from 'react-router';
import '../../App.css';

var base = Rebase.createClass({
  apiKey: "AIzaSyDmDmCpFzNhHt_Z-bizH-72EGIJeNmBkgg",
   authDomain: "nyne-bc248.firebaseapp.com",
   databaseURL: "https://nyne-bc248.firebaseio.com",
   storageBucket: "nyne-bc248.appspot.com",
   messagingSenderId: "938488675521"
});

var loginBoxes = [];
var fbBtn = require('../../icons/fblogin.png');



class Login extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      data: []
    };
    this.createUser = this.createUser.bind(this);
    this.authListener = this.authListener.bind(this);
  }

authListener(){

var self = this;
  this.fireBaseListener = base.auth().onAuthStateChanged(function(user) {
  if (user) {
    var foo = [];

    for(var i = 0; i < 9; i++)
    foo.push({content: <Box name={''} bg={''} handleClick={self.handleClick} handleImage={self.handleImage}/>});

var grid = JSON.stringify(foo);
    var userEmail = user.email;
    var userID = user.uid;

    base.fetch('users', {
  context: this,
  asArray: false,
  queries: {
  orderByKey: true,
  equalTo: userID
},
  then(data){
    if(data)
    {
      browserHistory.push('/profile/UrnEQJcAMBg7EB2HCSYRB4wUkEF3');

    }
    else{
      base.post(`users/${userID}/`, {
        data: {grid: grid, email: userEmail},
        then(err){
          if(!err){


            browserHistory.push('/profile/UrnEQJcAMBg7EB2HCSYRB4wUkEF3');
          }
        }
      });
    }
  }
});


  } else {
  }
});
}

  createUser(){
    var authHandler = function(error, user) {
      if(error){
        console.log(error);
      }
      else
      console.log(user);
    }
    //basic
    base.authWithOAuthPopup('facebook', authHandler);
  }

  componentWillMount(){




  }

  componentWillUnmount(){

this.fireBaseListener && this.fireBaseListener();
this.authListener = undefined;

  }


componentDidMount(){
  this.authListener();
  loginBoxes = [];
  for(var i = 0; i < 9; i++)
  {
    console.log(i);
    loginBoxes.push({content: ''});

    if(i == 1){
      loginBoxes[i] = {content: <div className="title-contain"><h1 className="app-title">Nyne</h1></div>};
    }


    if(i == 4){
      loginBoxes[i] = {content: (<div className="login-contain"><input type="text" placeholder="Username"/><input type="text" placeholder="Password"/><button className="login-btn">Log In</button></div>)};

    }

    if(i == 7){
      loginBoxes[i] = {content: (<div className="login-contain"><img className="fb-login" onClick={this.createUser} src={fbBtn}/><a className="signup-btn" href="">Sign Up</a></div>)};

    }

    this.setState({data: loginBoxes});
  }




}


  render(){

    var boxes = document.getElementsByClassName("draggable");

    for (var i = 0; i < boxes.length; i++) {
      boxes[i].style.height = window.getComputedStyle(boxes[i]).width;
    }
    return (
      <div>
        <DragSortableList items={this.state.data} dropBackTransitionDuration={1} moveTransitionDuration={0.5} type="grid"/>;
      </div>
    )
  }
};


export default Login;
